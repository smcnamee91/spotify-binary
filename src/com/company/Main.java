package com.company;

public class Main {

    public static void main(String[] args) {
        long start = 74;

        String bin;
        String reversedBin;
        long result;

        Main a = new Main();
        bin = a.convertToBinary(start);
        reversedBin = a.reverseString(bin);
        result = a.convertToDecimal(reversedBin);

        System.out.println(start);
        System.out.println(bin);
        System.out.println(reversedBin);
        System.out.println(result);


     }

    /**
     *
     * @param num
     * @return
     */
    public String convertToBinary(long num){
        String result = "";

        if(num <= 1) {
            return String.valueOf(num%2);
        }
        else{
            return convertToBinary(num/2) + (num%2);
        }
    }

    /**
     *
     * @param string
     * @return
     */
    public String reverseString(String string){
        StringBuilder result = new StringBuilder();

        for(int i = (string.length() - 1); i >= 0; i--){
            result.append(string.charAt(i));
        }

        return result.toString();
    }

    /**
     *
     * @param bin
     * @return
     */
    public long convertToDecimal(String bin){

        long result = 0;
        int pow = 0;

        for(long i = (bin.length() -1); i >= 0; i--){
            long x = (bin.charAt((int)i) - '0');
            result += (x * (Math.pow(2, pow)));
            pow++;
        }

        return result;
    }
}
