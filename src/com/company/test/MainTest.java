package com.company.test;

import com.company.Main;
import jdk.nashorn.internal.ir.annotations.Ignore;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Created by a543097 on 23/01/2015.
 */
public class MainTest {

    @Ignore
    public void convertToBinaryLowerBound(){
        String expected = "0001";

        assertEquals(expected, new Main().convertToBinary(1));
    }

    @Test
    public void convertToBinaryMixed(){
        String expected = "11010010";

        assertEquals(expected, new Main().convertToBinary(210));
    }

    @Test
    public void convertToBinaryByte(){
        String expected = "11111111";

        assertEquals(expected, new Main().convertToBinary(255));
    }

    @Test
    public void convertToBinaryUpperBound(){
        String expected = "111111111111111111111111111111111111111111111111111111111111111";

        assertEquals(expected, new Main().convertToBinary(9223372036854775807L));
    }

    @Test
    public void convertToDecimalLowerBound(){
        int expected = 1;

        assertEquals(expected, new Main().convertToDecimal("0001"));
    }

    @Test
    public void convertToDecimalByte(){
        int expected = 255;

        assertEquals(expected, new Main().convertToDecimal("11111111"));
    }

    @Test
    public void convertToDecimalMixed(){
        int expected = 210;

        assertEquals(expected, new Main().convertToDecimal("11010010"));
    }

    @Test
    public void convertToDecimalUpperBound(){
        long expected = 9223372036854775807L;

        assertEquals(expected, new Main().convertToDecimal("111111111111111111111111111111111111111111111111111111111111111"));
    }
}
